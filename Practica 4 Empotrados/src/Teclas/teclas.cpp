/* 
 * File:   teclas.cpp
 * Author: aicarmon
 *
 * Created on 14 de octubre de 2015, 17:16
 */

#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include "xc.h"
#include "teclas.h"

using namespace std;


    
    Tecla::Tecla(char tecla){
        
        eixidab = tecla;
        
        if(tecla == 1){
            
            TECLA_1_PullUp = 1;
                        
        }else if(tecla == 2){
            
            TECLA_2_PullUp = 1;
                       
        }else if(tecla == 3){
            
            TECLA_3_PullUp = 1;
            
        }
    }
    
    char Tecla::pulsada(void){
        
        if(eixidab == 1){            
            if(TECLA_1_Pulsada == 0){
                return 1;
            }
        }else if(eixidab == 2){
            if(TECLA_2_Pulsada == 0){
                return 1;
            }
        }else if(eixidab == 3){
            if(TECLA_3_Pulsada == 0){
                return 1;
            }
        }
    }

