/* 
 * File: teclas.h
 * Author: Aitor Carricondo
 * Comments:
 * Revision history: 
 */

#define TECLA_1 LATBbits.LATB14
#define TECLA_1_T TRISBbits.TRISB14
#define TECLA_1_PullUp CNPUBbits.CNPUB14
#define TECLA_1_Pulsada PORTBbits.RB14

#define TECLA_2 LATBbits.LATB12
#define TECLA_2_T TRISBbits.TRISB12
#define TECLA_2_PullUp CNPUBbits.CNPUB12
#define TECLA_2_Pulsada PORTBbits.RB12

#define TECLA_3 LATBbits.LATB7
#define TECLA_3_T TRISBbits.TRISB7
#define TECLA_3_PullUp CNPUBbits.CNPUB7
#define TECLA_3_Pulsada PORTBbits.RB7

class Tecla{

    private:
        unsigned char eixidab;
        
        
    public:
        //Constructor de la clase
        //tecla vale 1,2 o 3 segun la tecla que pulsemos en la placa
        Tecla(char tecla);
        
        //Esta funcion devuelve un 1 cuando se pulsa una tecla
        char pulsada(void);
};