/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef IN_OUT_H
#define	IN_OUT_H

void init_in_out(void);

#endif /* IN_OUT_H */