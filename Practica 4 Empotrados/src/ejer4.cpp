/* 
 * File:   ejer4.cpp
 * Author: aicarmon
 *
 * Created on 7 de octubre de 2015, 15:42
 */

#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include "xc.h"
#include "config/config.h"
#include "in_out/in_out.h"
#include "Teclas/teclas.h"
#include "Leds/leds.h"

using namespace std;

int main(int argc, char** argv) {
    
    init_in_out();
    
    Led LedGreen(1);
    Led LedRed(2);
    
    Tecla Tecla1(1);
    Tecla Tecla2(2);
    Tecla Tecla3(3);
    
    while(1)
    {

        /*LedGreen.LedOn();
        LedRed.LedOn();*/
        
        if(Tecla1.pulsada()){
            LedGreen.LedOn();
        }
        
        if(Tecla2.pulsada()){
            LedGreen.LedOff();
            LedRed.LedOff();
        }
        
        if(Tecla3.pulsada()){
            LedRed.LedOn();
        }
        
    }
}



