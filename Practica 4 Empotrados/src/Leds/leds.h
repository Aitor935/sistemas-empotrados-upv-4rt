/* 
 * File: leds.h
 * Author: Aitor Carricondo
 * Comments:
 * Revision history: 
 */

#define LED_1 LATAbits.LATA4
#define LED_1_T TRISAbits.TRISA4
#define LED_2 LATBbits.LATB15
#define LED_2_T TRISBbits.TRISB15
#define LED_TEST_OFF 0
#define LED_TEST_ON 1
#define OUT_1 LATAbits.LATA4
#define OUT_2 LATBbits.LATB15


class Led{

    private:
        unsigned char eixidab;
        
        
    public:        
        //Constructor de la clase
        //eixida vale 1 o 2 segun el led que queramos manejar.El 1 es el verde y el 2 es el rojo
        Led(char eixida);
        
        //Esta funcion enciende el Led de forma permanente
        void LedOn(void);        
        
        //Esta funcion apaga el Led de forma permanente
        void LedOff(void);
};