/* 
 * File:   led.cpp
 * Author: aicarmon
 *
 * Created on 14 de octubre de 2015, 17:03
 */

#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include "xc.h"
#include "leds.h"

using namespace std;

    

    Led::Led(char eixida){        
        
        eixidab = eixida;
        
        if (eixida == 1)
        {
            LED_1 = 0;   //LATAbits.LATA4 = 0;
            LED_1_T = 0; //TRISAbits.TRISA4 = 0;
        }
        else if (eixida == 2)
        {
            LED_2 = 0;   //LATBbits.LATB15 = 0;
            LED_2_T = 0; //TRISBbits.TRISB15 = 0;
        }
    }       
    
    
    void Led::LedOn(){
        
        if(eixidab == 1){            
            LED_1 = LED_TEST_ON;            
        }else if(eixidab == 2){        
            LED_2 = LED_TEST_ON;
        }
    }
    
    void Led::LedOff(){
        
        if(eixidab == 1){
            LED_1 = LED_TEST_OFF;
        }else if(eixidab == 2){
            LED_2 = LED_TEST_OFF;
        }        
    }




