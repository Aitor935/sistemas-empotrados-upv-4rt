/* 
 * File:   in_out.cpp
 * Author: aicarmon
 *
 * Created on 7 de octubre de 2015, 15:48
 */

#include <cstdlib>
#include "xc.h"
#include "in_out.h"

//using namespace std;

/*
 * 
 */

void init_in_out(void)
{
    
        //#define LED_ROIG = TRISBbits.TRISB15;
        //#define LED_VERD = TRISAbits.TRISA4;
    
        ANSELA = 0x0000;                    //Ning�n Pin A Como entrada analogica
        PORTA = 0x0000;                     //Valor por defecto para las salidas
        TRISA = 0b1111111111101111;         //Todos entradas menos el RB4, por eso vale 0
        
        ANSELB = 0x0000;                    //Ning�n Pin B Como entrada analogica
        PORTB = 0x0000;                     //Valor por defecto para las salidas
        TRISB = 0b0111111111111111;         //Todos entradas menos la RB15
        CNPUB = 0b0101000010000000;         //Pull-up para las entradas 14 12 y 7
        
}