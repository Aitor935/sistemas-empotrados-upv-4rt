/* 
 * File:   ejer3.cpp
 * Author: aicarmon
 *
 * Created on 7 de octubre de 2015, 15:42
 */

#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include "config/config.h"
#include "in_out/in_out.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    init_in_out();
    
    LATAbits.LATA4 = 0;
    LATBbits.LATB15 = 0;
    
    while(1){
    
        /*LATAbits.LATA4 = 1;
        for(long i= 0;i<10000000;i++){}
        LATAbits.LATA4 = 0;
        LATBbits.LATB15 = 1;
        for(long i= 0;i<10000000;i++){}
        LATAbits.LATA4 = 0;
        LATBbits.LATB15 = 0;
        for(long i= 0;i<10000000;i++){}*/
        
        
        
        if(PORTBbits.RB7 == 0){
            LATAbits.LATA4 = 1;
        }
        
        if(PORTBbits.RB14 == 0){
            LATBbits.LATB15 = 1;
        }
        
        if(PORTBbits.RB12 == 0){
            LATAbits.LATA4 = 0;
            LATBbits.LATB15 = 0;
        }
        
    
    
    }
}

