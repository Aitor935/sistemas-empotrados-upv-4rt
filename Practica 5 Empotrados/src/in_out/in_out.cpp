#include <xc.h>

#include "in_out.h"


void Init_In_Out::init_in_out(void)
{
   ANSELA=0x0000;   // No uso puertos analogicos
   LATA=0x0000;    //Por defecto las salidas estan a 0
   TRISA=0b1111111111101111;    //Solo RA4 es salida
   ANSELB=0x0000;   // No uso puertos analogicos
   LATB=0x0000;    //Por defecto las salidas estan a 0
   TRISB=0b0111111111111111;    //Solo RB15 es salida
   CNPUB=0b0101000010000000;   // PULL-UP BB14, 12 y 7
}