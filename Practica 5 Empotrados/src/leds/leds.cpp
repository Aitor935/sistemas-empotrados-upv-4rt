#include <xc.h>
#include "leds.h"

Led::Led(char eixida)
{
    if (eixida==1)
    {
        eixidab=1;
        LED_1_T = 0;
        LED_1 = LED_TEST_OFF;
    }
    else
    {
        eixidab=2;
        LED_2_T = 0;
	LED_2 = LED_TEST_OFF;
    }
    
}

void Led::LedOn(void)
{
    if (eixidab==1)
    {
         LED_1=LED_TEST_ON;
    }
    else if (eixidab==2)
    {
         LED_2=LED_TEST_ON;
    }
}
void Led::LedOff(void)
{
     if (eixidab==1)
    {
         LED_1=LED_TEST_OFF;
    }
    else if (eixidab==2)
    {
         LED_2=LED_TEST_OFF;
    }
}
