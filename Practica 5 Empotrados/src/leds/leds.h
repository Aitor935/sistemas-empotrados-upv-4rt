/* 
 * File:   leds.h
 * Author: jrufino
 *
 * Created on 19 de octubre de 2015, 18:55
 */

#ifndef LEDS_H
#define	LEDS_H



#define LED_1 LATAbits.LATA4
#define LED_1_T TRISAbits.TRISA4
#define LED_2 LATBbits.LATB15
#define LED_2_T TRISBbits.TRISB15
#define LED_TEST_OFF 0
#define LED_TEST_ON 1
#define OUT_1 LATAbits.LATA4
#define OUT_2 LATBbits.LATB15



class Led {
    private:
       
        unsigned char eixidab;

    public:
        Led(char eixida);
        //Constructor de la clase
        //Inicializa el led para que este parpadeando continuamente 50 ms en cada segundo.
        // eixida vale 1 o 2 segun el led que queramos manejar. El 1 es el verde y el 2 el rojo

        void LedOn(void);
        // Esta funcion enciende el led de forma permanente
        // No se debe usar simultaneamente con "refersca"

        void LedOff(void);
        // Esta funcion apaga el led de forma permanente
        // No se debe usar simultaneamente con "refersca"

};


#endif	/* LEDS_H */

