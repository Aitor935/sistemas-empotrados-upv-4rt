/* 
 * File:   ejer4.cpp
 * Author: Aitor Carricondo
 *
 * Created on 7 de octubre de 2015, 16:17
 */
#include <xc.h>
#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include "./config/config.h"
#include "./in_out/in_out.h"
#include "./leds/leds.h"
#include "./teclas/teclas.h"
#include <sys/attribs.h>

using namespace std;

    Init_In_Out init_in_out();

    Led LedGreen(1);
    Led LedRed(2);   
    
    Tecla Tecla1(1);
    Tecla Tecla2(2);
    Tecla Tecla3(3);

int main(int argc, char** argv) {   
    
    
    
    LedRed.LedOff();
    LedGreen.LedOff();
    
    Tecla1.teclaInterrupcion();     // Fer que la tecla 1 vaja per interrupció
    Tecla3.teclaInterrupcion();     // fer que la tecla 3 vaja per interrupció
    
    INTCONSET = _INTCON_MVEC_MASK;  /*Configure Interrupt Controller for multi-vector mode*/
    __asm__ volatile("ei");         /*Enable Interrupts*/
    
    
    
    while(1)
    {
         
    }

    return 0;
}

