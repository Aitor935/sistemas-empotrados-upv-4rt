/* 
 * File:   teclas.h
 * Author: jrufino
 *
 * Created on 19 de octubre de 2015, 19:49
 */

#ifndef TECLAS_H
#define	TECLAS_H
#include <xc.h>

#define PUL_1     PORTBbits.RB14
#define PUL_2     PORTBbits.RB12
#define PUL_3     PORTBbits.RB7
#define PULSADO   0


class Tecla {
    private:
        unsigned char numero;
        unsigned char nteclab;
    public:
        
        Tecla(unsigned char num);           // Se le pasa como parametro la tecla que queremos.
                                            // Puede ser 1, 2 o 3
        unsigned char pulsada();            // Devuelve 1 si la tecla se ha pulsado desde la ultima vez que se
                                            // les ha preguntado.
        
        unsigned char pulsada_ahora(void);  // Devuelve un 1 si la tecla esta pulsada ahora.
        
        void teclaInterrupcion();           // Solo funciona con las teclas 1 y 3. A partir de ese momento
                                            // funcionan esas teclas por interrupcion.
        
        unsigned char variable_auxiliar;
        
};


#endif	/* TECLAS_H */




