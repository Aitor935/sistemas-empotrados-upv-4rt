
#include <xc.h>
#include <cstdlib>
#include <proc/p32mx150f128b.h>
#include <sys/attribs.h>
#include "xc.h"
#include "teclas.h"
#include "../main.h"

Tecla::Tecla(unsigned char num)
{
    numero=num;
    
    if (num==1)
    {
        CNPUBbits.CNPUB14=1;
    }
    else if (num==2)
    {
        CNPUBbits.CNPUB12=1;
    }
    else if (num==3)
    {
        CNPUBbits.CNPUB7=1;
    }
}

unsigned char Tecla::pulsada(void)
{

    if (IFS0bits.INT4IF == 1)
    {
        IFS0bits.INT4IF = 0;
    }
    else if (IFS0bits.INT1IF == 1)
    {
        IFS0bits.INT1IF == 1;
    }
}



unsigned char Tecla::pulsada_ahora(void)
{
    unsigned char valor;
    if (numero==1)
    {
        if (PUL_1 == PULSADO)
        {
            valor=1;
        }
        else
        {
            valor=0;
        }
    }
    else if (numero==2)
    {
    if (PUL_2 == PULSADO)
        {
            valor=1;
        }
        else
        {
            valor=0;
        }
    }
    else if (numero==3)
    {
        if (PUL_3 == PULSADO)
        {
            valor=1;
        }
        else
        {
            valor=0;
        }
    }
}

void Tecla::teclaInterrupcion(void){

    if(variable_auxiliar == '1'){
        
        INT4R = 0b0100;             // Asigna a la int4 el pin 0100
        IPC4bits.INT4IP = 0b100;    // Prioridad de la interrupción a 5
        INTCONbits.INT4EP = 0;      // Interrupción por flanco negativo
        IEC0bits.INT4IE = 1;        // Habilitamos la interrupción
        IFS0bits.INT4IF = 0;        // Borramos el flag
    }
    
    if(variable_auxiliar == '3'){
        
        INT1R = 0b0001;             // Asigna a la int4 el pin 0100
        IPC1bits.INT1IP = 0b100;    // Prioridad de la interrupción a 5
        INTCONbits.INT1EP = 0;      // Interrupción por flanco negativo
        IEC0bits.INT1IE = 1;        // Habilitamos la interrupción
        IFS0bits.INT1IF = 0;        // Borramos el flag
    }

}


extern "C"
{
    //Esta es la funcion que atiende a la interrupcion del pin INT1
    void __ISR(_EXTERNAL_1_VECTOR,IPL5AUTO)_INT1Handler(void){
        
        Tecla1.variable_auxiliar = 1;
        IFS0bits.INT1IF = 0; // Borra el flag de interrupcion. Si no se borra no habra
                             // mas interrupciones de este pin
    }
    
    //Esta es la funcion que atiende a la interrupcion del pin INT4
    void __ISR(_EXTERNAL_4_VECTOR,IPL5AUTO)_INT4Handler(void){
        
        Tecla3.variable_auxiliar = 3;
        IFS0bits.INT4IF= 0; // Borra el flag de interrupcion. Si no se borra no habra
                            // mas interrupciones de este pin
    }
}

